<?php
/**
 * Chimneys
 *
 * @package Coordinator\Modules\Chimneys
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

$module_name="chimneys";
$module_repository_url="https://bitbucket.org/SISCAS/cf-ext-chimneys/";
$module_repository_version_url="https://bitbucket.org/SISCAS/cf-ext-chimneys/raw/master/VERSION.txt";
$module_required_modules=array("databases");
