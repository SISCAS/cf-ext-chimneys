<?php
/**
 * Chimneys - Template
 *
 * @package Coordinator\Modules\Chimneys
 * @company Cogne Acciai Speciali s.p.a
 */

// build application
$app=new strApplication();
// build nav
$nav=new strNav("nav-tabs");
// dashboard
$nav->addItem(api_icon("fa-th-large",null,"hidden-link"),api_url(["scr"=>"dashboard"]));
// management
if(api_checkAuthorization("chimneys-manage")){$nav->addItem(api_text("nav-management"),api_url(["scr"=>"management"]));}
$nav->addItem(api_text("nav-view"),api_url(["scr"=>"view"]));
if(SCRIPT=="view"){$nav->addItem(api_text("nav-filters"),"#modal_view-filters",true,null,null,"data-toggle='modal'");}
// add nav to html
$app->addContent($nav->render(false));
