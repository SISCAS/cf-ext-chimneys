<?php
/**
 * Chimneys - Web Service
 *
 * @package Coordinator\Modules\Chimneys
 * @author  Manuel Zavatta <manuel.zavatta@gmail.com>
 * @link    http://www.coordinator.it
 */

// check for actions
if(!defined('ACTION')){die("ERROR EXECUTING WEB SERVICE: The action was not defined");}

// build return class
$return=new stdClass();
$return->error=false;
$return->errors=array();
$return->result=array();

// switch action
switch(ACTION){
	// measurements
	case "measurement_list":measurement_list($return);break;
	// default
	default:
		// action not found
		$return->error=true;
		$return->errors[]=array("action_not_found"=>"The action \"".ACTION."\" was not found in \"".MODULE."\" web service");
}

// encode or debug and return
if(!DEBUG){header("Content-Type: application/json; charset=utf-8");}else{api_dump($return,"RETURN");}
echo json_encode($return,JSON_PRETTY_PRINT);

/**
 * Measurement List
 *
 * tipologia={1|2}
 * camino=E37
 * camino[]=E37
 * camino[]=E38
 * stato={1|2}
 * impianto={1|2|3}
 * data_min=2021-01-01
 * data_max=2021-12-31
 *
 * webservice.php?mod=chimneys&act=measurement_list&token=f938c61468e486f34ddf9d12f9b7c3b0&tipologia=2&camino=E37
 *
 */
function measurement_list(&$return){
	// load configuration
	$chimneys_cfg=new cChimneysConfiguration();
	// check configuration
	if(!$chimneys_cfg->exists()){
		// configuration not found
		$return->error=true;
		$return->errors[]=array("configuration_not_found"=>"The module configuration was not found");
		// return
		return $return;
	}
	// check for token
	if($chimneys_cfg->token!=$_REQUEST["token"]){
		// token not match
		$return->error=true;
		$return->errors[]=array("token_error"=>"The provided token is not valid");
		// return
		return $return;
	}
	// connect to datasource
	try{
		$database_obj=new cDatabasesDatabase($chimneys_cfg->getDatasource());
		$database_obj->connect();
	}
	catch(Exception $e){
		api_dump($e->getMessage());
		// connection error
		$return->error=true;
		$return->errors[]=array("database_connection_error"=>"An error occurred while connecting to the database");
		// return
		return $return;
	}
	// acquire variables
	$r_tipologia=$_REQUEST["tipologia"];
	$r_data_min=(strlen($_REQUEST["data_min"])==10?$_REQUEST["data_min"]:date("Y-m-d"));
	$r_ora_min=(strlen($_REQUEST["ora_min"])==8?$_REQUEST["ora_min"]:"00:00:00");
	$r_data_max=(strlen($_REQUEST["data_max"])==10?$_REQUEST["data_max"]:null);
	$r_ora_max=(strlen($_REQUEST["ora_max"])==8?$_REQUEST["ora_max"]:"23:59:59");
	$r_camino_array=(is_array($_REQUEST["camino"])?$_REQUEST["camino"]:array_filter(array($_REQUEST["camino"])));
	$r_stato_array=(is_array($_REQUEST["stato"])?$_REQUEST["stato"]:array_filter(array($_REQUEST["stato"])));
	$r_impianto_array=(is_array($_REQUEST["impianto"])?$_REQUEST["impianto"]:array_filter(array($_REQUEST["impianto"])));
	// check for date min
	if(api_date_difference($r_data_min,date("Y-m-d"))>31){
		// too old
		$return->error=true;
		$return->errors[]=array("date_min_too_old"=>"The minimum date cannot be less than ".date("Y-m-d",strtotime("-31 days")));
		// return
		return $return;
	}
	// check required parameters
	if(!$r_tipologia){
		// connection error
		$return->error=true;
		$return->errors[]=array("required_parameter_tipologia"=>"Tipologia parameter is mandatory");
		// return
		return $return;
	}
	// build query
	$query="SELECT * FROM `rilevazioni`";
	$query.="  WHERE `tipologia`='".$r_tipologia."'";
	if($r_data_min){$query.="   AND `dataora`>='".$r_data_min." ".$r_ora_min."'";}
	if($r_data_max){$query.="   AND `dataora`<='".$r_data_max." ".$r_ora_max."'";}
	if(count($r_camino_array)){$query.="   AND `camino` IN ('".implode("','",$r_camino_array)."')";}
	if(count($r_stato_array)){$query.="   AND `stato` IN ('".implode("','",$r_stato_array)."')";}
	if(count($r_impianto_array)){$query.="   AND `impianto` IN ('".implode("','",$r_impianto_array)."')";}
	api_dump(str_replace("  ","\n",$query));
	// get records
	try{$records_array=$database_obj->select($query);}
	catch(Exception $e){
		api_dump($e->getMessage());
		// connection error
		$return->error=true;
		$return->errors[]=array("database_query_error"=>"An error occurred while quering to the database");
		// return
		return $return;
	}
	// build measurements array
	$measurements_array=array();
	foreach($records_array as $record_fobj){
		// build measurement
		$measurement=new stdClass();
		$measurement->dataora=$record_fobj->dataora;
		$measurement->camino=$record_fobj->camino;
		$measurement->stato=$record_fobj->stato;
		$measurement->impianto=$record_fobj->impianto;
		$measurement->tipologia=$record_fobj->tipologia;
		$measurement->valore=round($record_fobj->valore);
		$measurement->unita=$record_fobj->unita;
		// add measurement to measurements array
		$measurements_array[]=$measurement;
	}
	// add measurement to result
	$return->result["measurements"]=$measurements_array;
	// return
	return $return;
}
