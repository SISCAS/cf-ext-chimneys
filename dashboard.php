<?php
/**
 * Chimneys - Dashboard
 *
 * @package Coordinator\Modules\Chimneys
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("databases"));
// build dashboard object
$dashboard=new strDashboard();
$dashboard->addTile(api_url(["scr"=>"view"]),api_text("view"),api_text("view-description"),true,"1x1","fa-file-text-o");
// build grid object
$grid=new strGrid();
$grid->addRow();
$grid->addCol($dashboard->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
