<?php
/**
 * Chimneys - Reports View
 *
 * @package Coordinator\Modules\Chimneys
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("chimneys-usage","dashboard");
// include module report
require_once(MODULE_PATH."template.inc.php");
// load configuration
$chimneys_cfg=new cChimneysConfiguration();
if(!$chimneys_cfg->exists()){api_alerts_add(api_text("cChimneysConfiguration-alert-exists"),"danger");api_redirect(api_url(["scr"=>"dashboard"]));}
// set application title
$app->setTitle(api_text("view"));
//api_dump($_REQUEST);
// definitions
$records_array=array();
// connect to datasource
$database_obj=new cDatabasesDatabase($chimneys_cfg->getDatasource());
try{$database_obj->connect();}
catch(Exception $e){
	if(DEBUG){api_alerts_add($e->getMessage(),"danger");}
	else{api_alerts_add(api_text("cDatabasesDatabase-alert-connection-failed"),"danger");}
	api_redirect(api_url(["scr"=>"dashboard"]));
}
// get trascodifiche
$camino_array=$database_obj->select("SELECT DISTINCT `camino` FROM `rilevazioni`");
$tipologia_array=$database_obj->select("SELECT * FROM `trascodifiche` WHERE `campo`='tipologia'");
$stato_array=$database_obj->select("SELECT * FROM `trascodifiche` WHERE `campo`='stato'");
$impianto_array=$database_obj->select("SELECT * FROM `trascodifiche` WHERE `campo`='impianto'");
// get filters
$r_tipologia=$_REQUEST["tipologia"];
$r_camino_array=(is_array($_REQUEST["camino"])?$_REQUEST["camino"]:array());
$r_stato_array=(is_array($_REQUEST["stato"])?$_REQUEST["stato"]:array());
$r_impianto_array=(is_array($_REQUEST["impianto"])?$_REQUEST["impianto"]:array());
$r_data_range=(is_array($_REQUEST["data_range"])?$_REQUEST["data_range"]:array(date('Y-m-d')));
/**
 * Filters
 */
// build form
$form=new strForm("#","GET",null,null,"chimneys__view-filters_form");
// fields
$form->addField("hidden","mod",null,"chimneys");
$form->addField("hidden","scr",null,"view");
$form->addField("radio","tipologia",api_text("view-fl-tipologia"),$r_tipologia,null,null,"radio-inline",null,"required");
foreach(api_transcodingsFromObjects($tipologia_array,"chiave","valore") as $value=>$label){if(!$value){continue;}$form->addFieldOption($value,$label);}
$form->addField("select","camino[]",api_text("view-fl-camino"),$r_camino_array,null,null,null,null,"multiple size='6'");
foreach(api_transcodingsFromObjects($camino_array,"camino","camino") as $value=>$label){$form->addFieldOption($value,$label);}
$form->addField("select","stato[]",api_text("view-fl-stato"),$r_stato_array,null,null,null,null,"multiple size='3'");
foreach(api_transcodingsFromObjects($stato_array,"chiave","valore") as $value=>$label){$form->addFieldOption($value,$label);}
$form->addField("select","impianto[]",api_text("view-fl-impianto"),$r_impianto_array,null,null,null,null,"multiple size='4'");
foreach(api_transcodingsFromObjects($impianto_array,"chiave","valore") as $value=>$label){$form->addFieldOption($value,$label);}
$form->addField("range_date","data_range",api_text("view-fl-dataora"),$r_data_range,null,null,null,null,"min='".date('Y-m-d',strtotime("-30 days"))."'");
// controls
$form->addControl("submit",api_text("form-fc-submit"));
$form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
// build modal
$modal=new strModal(api_text("view-modal-filters"),null,"view-filters");
$modal->setBody($form->render(1));
// add modal to application
$app->addModal($modal);
// check for required filters
if(!$r_tipologia){
	$app->addScript("$(function(){\$('#modal_view-filters').modal({show:true,backdrop:'static',keyboard:false});});");
}else{
	// build query where
	$query_where="`tipologia`='".$r_tipologia."'";
	if($r_data_range[0]){$query_where.="   AND LEFT(`dataora`,10)>='".$r_data_range[0]."'";}
	if($r_data_range[1]){$query_where.="   AND LEFT(`dataora`,10)<='".$r_data_range[1]."'";}
	if(count($r_camino_array)){$query_where.="   AND `camino` IN ('".implode("','",$r_camino_array)."')";}
	if(count($r_stato_array)){$query_where.="   AND `stato` IN ('".implode("','",$r_stato_array)."')";}
	if(count($r_impianto_array)){$query_where.="   AND `impianto` IN ('".implode("','",$r_impianto_array)."')";}
	try{
		// count records
		$records_count=(int)$database_obj->select("SELECT COUNT(`id`) AS `count` FROM `rilevazioni` AS `r` WHERE ".$query_where)[0]->count;
		//api_dump($count,"count");
		// build pagination
		$pagination=new strPagination($records_count);
		// build query
		$query="SELECT   `r`.*,   `s`.`valore` AS stato_txt,   `i`.`valore` AS impianto_txt  ";
		$query.="FROM `rilevazioni` AS `r`  ";
		$query.="LEFT JOIN `trascodifiche` AS `s` ON `s`.`campo`='stato' AND `s`.`chiave`=`r`.`stato`  ";
		$query.="LEFT JOIN `trascodifiche` AS `i` ON `i`.`campo`='impianto' AND `i`.`chiave`=`r`.`impianto`  ";
		$query.="WHERE ".$query_where."  ";
		$query.="ORDER BY `dataora` ASC  ";
		$query.=$pagination->getQueryLimits();
		//api_dump(str_replace("  ","\n  ",$query));
		// get records
		$records_array=$database_obj->select($query);
	}
	catch(Exception $e){if(DEBUG){api_alerts_add($e->getMessage(),"danger");}}
}
// build table
$table=new strTable(api_text("view-tr-unvalued"));
$table->addHeader(api_text("view-th-camino"),"nowrap");
$table->addHeader(api_text("view-th-dataora"),"nowrap");
$table->addHeader(api_text("view-th-stato"),"nowrap");
$table->addHeader(api_text("view-th-impianto"),"nowrap");
$table->addHeader(api_text("view-th-valore"),"nowrap text-right");
$table->addHeader(api_text("view-th-unita"),"nowrap");
// cycle all results
foreach($records_array as $record_fobj){
	// make row
	$table->addRow();
	$table->addRowField($record_fobj->camino,"nowrap");
	$table->addRowField($record_fobj->dataora,"nowrap");
	$table->addRowField($record_fobj->stato_txt,"nowrap");
	$table->addRowField($record_fobj->impianto_txt,"nowrap");
	$table->addRowField(number_format($record_fobj->valore,0,",","."),"nowrap text-right");
	$table->addRowField($record_fobj->unita,"nowrap");
}
// build grid
$grid=new strGrid();
$grid->addRow();
//$grid->addCol($filters_labels,"col-xs-12");
$grid->addRow();
$grid->addCol($table->render(),"col-xs-12");
$grid->addRow();
if(is_object($pagination)){$grid->addCol($pagination->render(),"col-xs-12");}
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
if(strlen($query)){api_dump(str_replace("  ","\n",$query),"query");}
