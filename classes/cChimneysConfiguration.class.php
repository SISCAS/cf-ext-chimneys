<?php
/**
 * Chimneys - Configuration
 *
 * @package Coordinator\Modules\Chimneys
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Chimneys, Configuration class
 */
class cChimneysConfiguration extends cConfiguration{

	/** Properties */
	protected $datasource;
	protected $token;

	/**
	 * Get Datasource
	 *
	 * @return cDatabasesDatasource
	 */
	public function getDatasource(){return new cDatabasesDatasource($this->datasource);}

}
