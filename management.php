<?php
/**
 * Chimneys - Management
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("chimneys-manage","dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("management"));
// load configuration
$chimneys_cfg=new cChimneysConfiguration();
if(!$chimneys_cfg->exists()){api_alerts_add(api_text("cChimneysConfiguration-alert-exists"),"warning");}
else{
	// build configuration description list
	$dl=new strDescriptionList("br","dl-horizontal");
	$dl->addElement(api_text("cConfiguration-label-store"),$chimneys_cfg->getStoreLabel());
}
// build form
$form=new strForm(api_url(["scr"=>"controller","obj"=>"cChimneysConfiguration","act"=>"store"]),"POST",null,null,"management-configuration_form");
// fields
$form->addField("select","datasource",api_text("cChimneysConfiguration-property-datasource"),$chimneys_cfg->datasource,api_text("cChimneysConfiguration-placeholder-datasource"),null,null,null,"required");
foreach(cDatabasesDatasource::availables(true) as $datasource_fobj){$form->addFieldOption($datasource_fobj->id,$datasource_fobj->getLabel());}
$form->addField("text","token",api_text("cChimneysConfiguration-property-token"),(strlen($chimneys_cfg->token)!=32||$_REQUEST["token"]=="new"?md5(time()):$chimneys_cfg->token),api_text("cChimneysConfiguration-placeholder-token"),null,null,null,"required".(!DEBUG?" readonly":null));
$form->addFieldAddonButton(api_url(["scr"=>"management","token"=>"new"]),api_text("management-fb-token"),"btn-primary");
$form->addControl("submit",api_text("form-fc-save"));
// build grid
$grid=new strGrid();
if(is_object($dl)){
	$grid->addRow();
	$grid->addCol($dl->render(),"col-xs-12");
}
$grid->addRow();
$grid->addCol($form->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
api_dump($chimneys_cfg,"configuration object");
