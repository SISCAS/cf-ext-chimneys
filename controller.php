<?php
/**
 * Chimneys - Controller
 *
 * @package Coordinator\Modules\Chimneys
 * @company Cogne Acciai Speciali s.p.a
 */

// debug
api_dump($_REQUEST,"_REQUEST");
// check if object controller function exists
if(function_exists($_REQUEST["obj"]."_controller")){
	// call object controller function
	call_user_func($_REQUEST["obj"]."_controller",$_REQUEST["act"]);
}else{
	api_alerts_add(api_text("alert_controllerObjectNotFound",[MODULE,$_REQUEST["obj"]."_controller"]),"danger");
	api_redirect("?mod=".MODULE);
}

/**
 * cChimneysConfiguration controller
 *
 * @param string $action Controller action
 */
function cChimneysConfiguration_controller($action){
	// check authorizations
	api_checkAuthorization("databases-manage","dashboard");
	// get object
	$configuration=new cChimneysConfiguration();
	api_dump($configuration,"configuration");
	// execution
	try{
		switch($action){
			case "store":
				$configuration->store($_REQUEST);
				api_alerts_add(api_text("cChimneysConfiguration-alert-stored"),"success");
				break;
			default:
				throw new Exception("Configuration action \"".$action."\" was not defined..");
		}
		// redirect
		api_redirect(api_return_url(["scr"=>"management"]));
	}catch(Exception $e){
		// dump, alert and redirect
		api_redirect_exception($e,api_url(["scr"=>"management"]),"cChimneysConfiguration-alert-error");
	}
}
